# Khaleesi

Khaleesi is a blog-aware or documentation-aware static site generator write in Ruby, looks like a text transformation engine to transforming your plain text into static websites. Supported markdown parser, series of decorators wrapping, code syntax highlighting, simple page script programming, page including, dataset traversing etc. Please visit my project's official site [khaleesi.vincestyling.com](http://khaleesi.vincestyling.com/) for more details.

This project has moved to [GitHub](https://github.com/vince-styling/khaleesi).

# License

Khaleesi is released under the MIT license. Please see the LICENSE file for more information.